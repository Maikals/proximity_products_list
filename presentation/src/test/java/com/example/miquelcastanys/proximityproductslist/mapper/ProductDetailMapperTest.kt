package com.example.miquelcastanys.proximityproductslist.mapper

import android.content.Context
import com.example.domain.entities.CarProduct
import com.example.domain.entities.ConsumerGoodProduct
import com.example.domain.entities.ServiceProduct
import com.example.miquelcastanys.proximityproductslist.UnitTest
import com.example.miquelcastanys.proximityproductslist.entities.ProductDetail
import com.example.miquelcastanys.proximityproductslist.entities.mappers.ProductDetailMapper
import com.nhaarman.mockito_kotlin.doReturn
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Assert.assertThat
import org.junit.Test
import org.mockito.Mock

class ProductDetailMapperTest : UnitTest() {

    @Mock
    private lateinit var context: Context

    private val carProduct = CarProduct("", "", "", "", "", "", "", 0, "", 0)
    private val consumerGoodProductProduct = ConsumerGoodProduct("", "", "", "", "", "", "", 0)
    private val serviceProduct =  ServiceProduct("", "", "", "", "", "", 0, "", 0)

    @Test
    fun carConvertToTest() {
        mockContext()

        val resultProductDetail = ProductDetailMapper.convertTo(carProduct, context)

        assertThatIsProductDetail(resultProductDetail)
        assert(resultProductDetail.fields.size == 4)
    }

    @Test
    fun consumerGoodConvertToTest() {
        mockContext()

        val resultConsumerGoodDetail = ProductDetailMapper.convertTo(consumerGoodProductProduct, context)

        assertThatIsProductDetail(resultConsumerGoodDetail)
        assert(resultConsumerGoodDetail.fields.size == 2)
    }

    @Test
    fun serviceConvertToTest() {
        mockContext()

        val resultServiceDetail = ProductDetailMapper.convertTo(serviceProduct, context)

        assertThatIsProductDetail(resultServiceDetail)
        assert(resultServiceDetail.fields.size == 3)
    }

    private fun mockContext() {
        doReturn("Unknown")
                .`when`(context)
    }

    private fun assertThatIsProductDetail(resultServiceDetail: ProductDetail) {
        assertThat(resultServiceDetail, `is`<Any>(instanceOf<Any>(ProductDetail::class.java)))
    }

}