package com.example.miquelcastanys.proximityproductslist.presenter

import com.example.domain.entities.CarProduct
import com.example.domain.entities.baseEntities.BaseProduct
import com.example.domain.interactor.GetProductDetailUseCase
import com.example.miquelcastanys.proximityproductslist.UnitTest
import com.example.miquelcastanys.proximityproductslist.entities.ProductObserver
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class ProductDetailPresenterTest : UnitTest() {
    private lateinit var presenter: ProductDetailPresenter
    @Mock
    private lateinit var productDetailUseCase: GetProductDetailUseCase
    @Mock
    private lateinit var productDetailView: ProductDetailView


    @Before
    fun setUp() {
        presenter = ProductDetailPresenter(productDetailUseCase)
        presenter.view = productDetailView
    }

    @Test
    fun startTest() {
        presenter.start("")

        verify(productDetailUseCase).execute(any(), any<ProductObserver<BaseProduct>>())
    }

    @Test
    fun onNextTest() {
        presenter.onNext(createProduct(), false)

        verify(presenter.view).provideContext()?.let{
            verify(presenter.view).hideEmptyView()
            verify(presenter.view).setData(any())
            verify(presenter.view).showProductInfo()
        }
    }

    @Test
    fun onErrorTest() {
        presenter.onError(Throwable())

        verify(presenter.view).hideProductInfo()
        verify(presenter.view).showEmptyView()
    }

    @Test
    fun onDestroyTest() {
        presenter.destroy()

        verify(productDetailUseCase).dispose()
    }

    private fun createProduct(): BaseProduct =
            CarProduct("",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    2,
                    "",
                    2)
}