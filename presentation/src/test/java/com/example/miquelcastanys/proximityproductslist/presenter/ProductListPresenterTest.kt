package com.example.miquelcastanys.proximityproductslist.presenter

import com.example.domain.entities.CarProduct
import com.example.domain.entities.baseEntities.BaseProduct
import com.example.domain.interactor.GetProductListUseCase
import com.example.miquelcastanys.proximityproductslist.UnitTest
import com.example.miquelcastanys.proximityproductslist.entities.ConsumerGoodsProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.ProductObserver
import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseListItem
import com.example.miquelcastanys.proximityproductslist.views.productsList.ProductListView
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.isNull
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class ProductListPresenterTest : UnitTest() {
    private lateinit var presenterWithMockedList: ProductListPresenter
    private lateinit var presenterWithoutMockedList: ProductListPresenter
    @Mock
    private lateinit var productListUseCase: GetProductListUseCase
    @Mock
    private lateinit var productListView: ProductListView
    @Mock
    private lateinit var productList: ArrayList<BaseListItem>

    @Before
    fun setUp() {
        presenterWithMockedList = ProductListPresenter(productListUseCase)
        presenterWithMockedList.view = productListView
        presenterWithMockedList.productList = productList

        presenterWithoutMockedList = ProductListPresenter(productListUseCase)
        presenterWithoutMockedList.view = productListView
        presenterWithoutMockedList.productList = createProductItemList()
    }

    @Test
    fun startTest() {
        presenterWithMockedList.start()

        verify(productListUseCase).execute(isNull(), any<ProductObserver<List<BaseProduct>>>())
    }

    @Test
    fun getProductListTest() {
        presenterWithMockedList.getProximityProductList(false)

        verify(productListUseCase).execute(isNull(), any<ProductObserver<List<BaseProduct>>>())
    }

    @Test
    fun onNextWithoutRefreshTest() {
        presenterWithMockedList.onNext(createProductList(), false)

        verify(presenterWithMockedList.productList, never()).clear()
        commonOnNextOkVerifications()
    }

    @Test
    fun onNextWithRefreshTest() {
        presenterWithMockedList.onNext(createProductList(), true)

        verify(presenterWithMockedList.productList).clear()
        commonOnNextOkVerifications()
    }

    private fun commonOnNextOkVerifications() {
        verify(presenterWithMockedList.view).hideEmptyView()
        verify(presenterWithMockedList.view).showProgressBar(false)
        verify(presenterWithMockedList.view).showRecyclerView()
        verify(presenterWithMockedList.view).provideContext()?.let {
            verify(presenterWithMockedList.productList).addAll(any())
        }
        verify(presenterWithMockedList.view).setItems(presenterWithMockedList.productList)
    }

    @Test
    fun onNextEmptyViewTest() {
        presenterWithMockedList.onNext(ArrayList(), false)

        commonEmptyListVerifications()
    }

    @Test
    fun onErrorTest() {
        presenterWithMockedList.onError(Throwable())

        commonEmptyListVerifications()
    }

    private fun commonEmptyListVerifications() {
        verify(presenterWithMockedList.view).hideRecyclerView()
        verify(presenterWithMockedList.view).showProgressBar(false)
        verify(presenterWithMockedList.view).showEmptyView()
        verify(presenterWithMockedList.productList).clear()
        verify(presenterWithMockedList.view).hideDistanceRangeView()
    }

    @Test
    fun calculateDistanceRangeTest() {
        val (initArray, finalArray) = createPositionArrays()

        presenterWithoutMockedList.calculateDistanceRange(initArray, finalArray)

        verify(presenterWithMockedList.view).setDistanceRange(any(), any())
        verify(presenterWithMockedList.view).showDistanceRangeView()
    }

    @Test
    fun onDestroyTest() {
        presenterWithMockedList.destroy()

        verify(productListUseCase).dispose()
    }

    private fun createPositionArrays(): Pair<IntArray, IntArray> {
        val initArray = IntArray(2)
        initArray[0] = 1
        initArray[1] = 0
        val finalArray = IntArray(2)
        finalArray[0] = 1
        finalArray[1] = 2
        return Pair(initArray, finalArray)
    }

    private fun createProductList(): List<BaseProduct> =
            listOf(createProduct(), createProduct(), createProduct())

    private fun createProduct(): BaseProduct =
            CarProduct("",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    2,
                    "",
                    2)

    private fun createProductItemList(): ArrayList<BaseListItem> =
            arrayListOf(createProductItem(), createProductItem(), createProductItem())

    private fun createProductItem(): BaseListItem =
            ConsumerGoodsProductListItem("", "", "", "", "", "", "", "4m")
}