package com.example.miquelcastanys.proximityproductslist.mapper

import android.content.Context
import com.example.domain.entities.AdvertisementProduct
import com.example.domain.entities.CarProduct
import com.example.domain.entities.ConsumerGoodProduct
import com.example.domain.entities.ServiceProduct
import com.example.miquelcastanys.proximityproductslist.UnitTest
import com.example.miquelcastanys.proximityproductslist.entities.AdvertisementProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.CarProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.ConsumerGoodsProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.ServiceProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.mappers.ProductListMapper
import com.nhaarman.mockito_kotlin.doReturn
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Assert.assertThat
import org.junit.Test
import org.mockito.Mock

class ProductListMapperTest : UnitTest() {

    companion object {
        private const val CAR_POSITION = 0
        private const val CONSUMER_GOOD_POSITION = 1
        private const val SERVICE_POSITION = 2
        private const val ADVERTISEMENT_POSITION = 3
    }

    @Mock
    private lateinit var context: Context

    private val productList = listOf(CarProduct("", "", "", "", "", "", "", 0, "", 0),
            ConsumerGoodProduct("", "", "", "", "", "", "", 0),
            ServiceProduct("", "", "", "", "", "", 0, "", 0),
            AdvertisementProduct("", "", ""))

    @Test
    fun convertToTest() {
        doReturn("Unknown")
                .`when`(context)

        val resultList = ProductListMapper.convertTo(productList, context)

        assertThat(resultList[CAR_POSITION], `is`<Any>(instanceOf<Any>(CarProductListItem::class.java)))
        assertThat(resultList[CONSUMER_GOOD_POSITION], `is`<Any>(instanceOf<Any>(ConsumerGoodsProductListItem::class.java)))
        assertThat(resultList[SERVICE_POSITION], `is`<Any>(instanceOf<Any>(ServiceProductListItem::class.java)))
        assertThat(resultList[ADVERTISEMENT_POSITION], `is`<Any>(instanceOf<Any>(AdvertisementProductListItem::class.java)))
    }

}