package com.example.miquelcastanys.proximityproductslist.observer

import com.example.domain.entities.baseEntities.BaseProduct
import com.example.miquelcastanys.proximityproductslist.UnitTest
import com.example.miquelcastanys.proximityproductslist.entities.ProductObserver
import com.example.miquelcastanys.proximityproductslist.interfaces.ObserverPresenterInterface
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class ObserverTest : UnitTest() {

    @Mock
    lateinit var observerPresenterInterface: ObserverPresenterInterface<List<BaseProduct>>
    @Mock
    lateinit var productList: List<BaseProduct>
    lateinit var productObserver: ProductObserver<List<BaseProduct>>

    @Before
    fun setUp() {
        productObserver = ProductObserver(observerPresenterInterface, false)
    }

    @Test
    fun onCompleteTest() {
        productObserver.onComplete()

        verify(observerPresenterInterface).onComplete()
    }

    @Test
    fun onNextTest() {
        productObserver.onNext(ArrayList())

        verify(observerPresenterInterface).onNext(any(), any())
    }

    @Test
    fun onErrorTest() {
        productObserver.onError(Throwable())

        verify(observerPresenterInterface).onError(any())
    }

}