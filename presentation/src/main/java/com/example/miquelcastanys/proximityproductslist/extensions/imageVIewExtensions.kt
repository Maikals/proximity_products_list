package com.example.miquelcastanys.proximityproductslist.extensions

import android.widget.ImageView
import com.example.miquelcastanys.proximityproductslist.entities.enumerations.ProductTypeEnum
import com.example.miquelcastanys.proximityproductslist.glideModule.GlideApp

fun ImageView.loadImageFromUrl(url: String, type: ProductTypeEnum) =
    GlideApp.with(this)
            .load(url)
            .placeholder(type.drawableId)
            .centerCrop()
            .into(this)
