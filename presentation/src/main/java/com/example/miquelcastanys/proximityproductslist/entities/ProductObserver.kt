package com.example.miquelcastanys.proximityproductslist.entities

import com.example.miquelcastanys.proximityproductslist.interfaces.ObserverPresenterInterface
import io.reactivex.observers.DisposableObserver


class ProductObserver<T>(private val observerPresenterInterface: ObserverPresenterInterface<T>, private val refreshList: Boolean = false) : DisposableObserver<T>() {
    override fun onComplete() {
        observerPresenterInterface.onComplete()
    }

    override fun onNext(result: T) {
        observerPresenterInterface.onNext(result, refreshList)
    }

    override fun onError(e: Throwable) {
        observerPresenterInterface.onError(e)
    }
}