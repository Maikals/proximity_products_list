package com.example.miquelcastanys.proximityproductslist.injector.component

import com.example.miquelcastanys.proximityproductslist.injector.PerFragment
import com.example.miquelcastanys.proximityproductslist.injector.modules.BaseFragmentModule
import com.example.miquelcastanys.proximityproductslist.injector.modules.ProductListModule
import com.example.miquelcastanys.proximityproductslist.views.productsList.ProductListFragment
import dagger.Subcomponent

@PerFragment
@Subcomponent(modules = [BaseFragmentModule::class, ProductListModule::class])
interface ProductListComponent {

    fun inject(productListFragment: ProductListFragment)

}