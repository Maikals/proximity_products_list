package com.example.miquelcastanys.proximityproductslist.views.viewHolders.baseViewHolders

import android.support.v7.widget.RecyclerView
import android.view.View


abstract class BaseViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    fun clearAnimation() {
        view.clearAnimation()
    }
}