package com.example.miquelcastanys.proximityproductslist.entities

import com.example.miquelcastanys.proximityproductslist.entities.enumerations.ProductTypeEnum
import java.util.*
import kotlin.collections.HashMap

data class ProductDetail(val name: String,
                         val imageUrl: String,
                         val price: String,
                         val description: String,
                         val distance: String,
                         val fields: List<String>,
                         val type: ProductTypeEnum)
