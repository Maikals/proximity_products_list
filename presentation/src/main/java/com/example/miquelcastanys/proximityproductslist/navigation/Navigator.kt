package com.example.miquelcastanys.proximityproductslist.navigation

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.annotation.StringRes
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.miquelcastanys.proximityproductslist.R
import com.example.miquelcastanys.proximityproductslist.views.productDetail.ProductDetailActivity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Navigator @Inject constructor() {

    fun toProductDetail(context: Context, productId: String, view: View) =
            context.startActivity(ProductDetailActivity.newIntent(context, productId),
                    createSharedTransition(context, view, R.string.transition_string).toBundle())

    fun toBrowser(context:Context, url: String) =
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))

    private fun createSharedTransition(context: Context, view: View, @StringRes transitionStringId: Int): ActivityOptionsCompat =
            ActivityOptionsCompat.makeSceneTransitionAnimation((context as? AppCompatActivity)!!,
                    view, // Starting view
                    context.getString(transitionStringId)!! // The String
            )
}