package com.example.miquelcastanys.proximityproductslist.injector.modules

import com.example.miquelcastanys.proximityproductslist.injector.PerFragment
import com.example.miquelcastanys.proximityproductslist.presenter.ProductDetailView
import com.example.miquelcastanys.proximityproductslist.views.productDetail.ProductDetailFragment
import dagger.Module
import dagger.Provides

@Module
class ProductDetailModule(private val productDetailFragment: ProductDetailFragment) {

    @Provides
    @PerFragment
    fun provideProductListView(): ProductDetailView {
        return productDetailFragment
    }
}