package com.example.miquelcastanys.proximityproductslist.views.productDetail


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.miquelcastanys.proximityproductslist.ProximityProductsListApplication
import com.example.miquelcastanys.proximityproductslist.R
import com.example.miquelcastanys.proximityproductslist.entities.ProductDetail
import com.example.miquelcastanys.proximityproductslist.entities.enumerations.EmptyViewEnumeration
import com.example.miquelcastanys.proximityproductslist.extensions.inflateFromLayout
import com.example.miquelcastanys.proximityproductslist.injector.modules.BaseFragmentModule
import com.example.miquelcastanys.proximityproductslist.injector.modules.ProductDetailModule
import com.example.miquelcastanys.proximityproductslist.interfaces.DetailActivityFragmentCommunicationInterface
import com.example.miquelcastanys.proximityproductslist.presenter.ProductDetailPresenter
import com.example.miquelcastanys.proximityproductslist.presenter.ProductDetailView
import com.example.miquelcastanys.proximityproductslist.views.baseViews.BaseFragment
import kotlinx.android.synthetic.main.fragment_product_detail.*
import kotlinx.android.synthetic.main.view_detail_field.view.*
import javax.inject.Inject


class ProductDetailFragment : BaseFragment(), ProductDetailView {

    private var listener: DetailActivityFragmentCommunicationInterface? = null
    @Inject
    lateinit var presenter: ProductDetailPresenter


    companion object {
        const val TAG = "ProductDetailFragment"
        private const val EXTRA_PRODUCT_ID = "ExtraProductId"

        fun newInstance(productId: String) =
                ProductDetailFragment().apply {
                    arguments = Bundle().apply {
                        putString(EXTRA_PRODUCT_ID, productId)
                    }
                }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as? DetailActivityFragmentCommunicationInterface
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragmentComponent()
        setEmptyView()
        presenter.start(arguments?.getString(EXTRA_PRODUCT_ID)!!)
    }

    private fun setEmptyView() {
        emptyView.fillViews(EmptyViewEnumeration.MOVIES_DETAIL_EMPTY_VIEW)
    }

    override fun setupFragmentComponent() {
        ProximityProductsListApplication
                .applicationComponent
                .plus(BaseFragmentModule(context!!), ProductDetailModule(this))
                .inject(this)
    }

    override fun hideEmptyView() {
        emptyView.visibility = View.GONE
    }

    override fun showEmptyView() {
        emptyView.visibility = View.VISIBLE
    }

    override fun showProductInfo() {
        productInfoContainer.visibility = View.VISIBLE
    }

    override fun hideProductInfo() {
        productInfoContainer.visibility = View.GONE
    }
    override fun setData(productDetail: ProductDetail) {
        listener?.setParameters(productDetail.name, productDetail.imageUrl, productDetail.type)
        setViews(productDetail)
    }

    private fun setViews(productDetail: ProductDetail) {
        productPrice.text = productDetail.price
        distanceInMeters.text = productDetail.distance
        productDescription.text = productDetail.description
        setFields(productDetail)
    }

    private fun setFields(productDetail: ProductDetail) {
        productDetail.fields.forEach {
            inflateProductFields(it)
        }
    }

    private fun inflateProductFields(it: String) {
        val inflateFromLayout = fieldsContainer.inflateFromLayout(R.layout.view_detail_field)
        inflateFromLayout.fieldText.text = it
        fieldsContainer.addView(inflateFromLayout)
    }

    override fun provideContext(): Context? = context

}
