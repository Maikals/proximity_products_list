package com.example.miquelcastanys.proximityproductslist.entities.baseEntities


abstract class BaseProductListItem(val id: String,
                                   val image: String,
                                   val price: String,
                                   val name: String,
                                   val description: String,
                                   val distanceInMeters: String) : BaseListItem()