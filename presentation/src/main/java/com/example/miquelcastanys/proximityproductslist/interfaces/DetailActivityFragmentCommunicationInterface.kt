package com.example.miquelcastanys.proximityproductslist.interfaces

import com.example.miquelcastanys.proximityproductslist.entities.enumerations.ProductTypeEnum
import com.example.miquelcastanys.proximityproductslist.interfaces.base.BaseActivityFragmentCommunicationInterface


interface DetailActivityFragmentCommunicationInterface : BaseActivityFragmentCommunicationInterface {

    fun setParameters(name: String, imageUrl:String, typeEnum: ProductTypeEnum)
}