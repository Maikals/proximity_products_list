package com.example.miquelcastanys.proximityproductslist.views.viewHolders

import android.view.View
import com.example.miquelcastanys.proximityproductslist.entities.ConsumerGoodsProductListItem
import com.example.miquelcastanys.proximityproductslist.interfaces.OnListItemClickInterface
import com.example.miquelcastanys.proximityproductslist.views.viewHolders.baseViewHolders.BaseProductViewHolder
import kotlinx.android.synthetic.main.list_item_consumer_good.view.*


class ConsumerGoodProductViewHolder(view: View, listener: OnListItemClickInterface.AdapterSide)
    : BaseProductViewHolder(view, listener) {

    fun bindView(consumerGoodsProductListItem: ConsumerGoodsProductListItem) {
        super.bindView(consumerGoodsProductListItem)
        view.consumerGoodCategory.text = consumerGoodsProductListItem.category
        view.consumerGoodColor.text = consumerGoodsProductListItem.color
        view.consumerGoodDescription.text = consumerGoodsProductListItem.description
    }
}