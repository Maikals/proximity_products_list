package com.example.miquelcastanys.proximityproductslist.injector.component

import com.example.miquelcastanys.proximityproductslist.injector.modules.*
import com.example.miquelcastanys.proximityproductslist.views.baseViews.BaseActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, ProductListApiModule::class, ProductDetailApiModule::class])
interface ApplicationComponent {
    fun inject(baseActivity: BaseActivity)

    fun plus(baseFragmentModule: BaseFragmentModule, productListModule: ProductListModule) : ProductListComponent

    fun plus(baseFragmentModule: BaseFragmentModule, productDetailModule: ProductDetailModule): ProductDetailComponent
}