package com.example.miquelcastanys.proximityproductslist.views.viewHolders

import android.view.View
import com.example.miquelcastanys.proximityproductslist.entities.CarProductListItem
import com.example.miquelcastanys.proximityproductslist.interfaces.OnListItemClickInterface
import com.example.miquelcastanys.proximityproductslist.views.viewHolders.baseViewHolders.BaseProductViewHolder
import kotlinx.android.synthetic.main.list_item_car.view.*


class CarProductViewHolder(view: View, listener: OnListItemClickInterface.AdapterSide)
    : BaseProductViewHolder(view, listener) {
    fun bindView(carProductListItem: CarProductListItem) {
        super.bindView(carProductListItem)
        view.carBrand.text = carProductListItem.brand
        view.carDescription.text = carProductListItem.description
        view.carGearbox.text = carProductListItem.gearbox
        view.carMotor.text = carProductListItem.motor
        view.carKm.text = carProductListItem.km
    }
}