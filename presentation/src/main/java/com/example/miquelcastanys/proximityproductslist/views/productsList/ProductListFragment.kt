package com.example.miquelcastanys.proximityproductslist.views.productsList

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.miquelcastanys.proximityproductslist.ProximityProductsListApplication
import com.example.miquelcastanys.proximityproductslist.R
import com.example.miquelcastanys.proximityproductslist.adapters.ProductListAdapter
import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseListItem
import com.example.miquelcastanys.proximityproductslist.entities.enumerations.EmptyViewEnumeration
import com.example.miquelcastanys.proximityproductslist.injector.modules.BaseFragmentModule
import com.example.miquelcastanys.proximityproductslist.injector.modules.ProductListModule
import com.example.miquelcastanys.proximityproductslist.interfaces.OnListItemClickInterface
import com.example.miquelcastanys.proximityproductslist.presenter.ProductListPresenter
import com.example.miquelcastanys.proximityproductslist.views.baseViews.BaseFragment
import kotlinx.android.synthetic.main.fragment_products_list.*
import javax.inject.Inject


class ProductListFragment : BaseFragment(), ProductListView, OnListItemClickInterface.ViewSide {

    @Inject
    lateinit var productListPresenter: ProductListPresenter

    private val gridLayoutManager = StaggeredGridLayoutManager(NUMBER_OF_COLUMNS, StaggeredGridLayoutManager.VERTICAL)
    private val onScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            val firstVisible = IntArray(NUMBER_OF_COLUMNS)
            val lastVisible = IntArray(NUMBER_OF_COLUMNS)

            productListPresenter.calculateDistanceRange(gridLayoutManager.findFirstVisibleItemPositions(firstVisible),
                    gridLayoutManager.findLastVisibleItemPositions(lastVisible))
        }
    }

    companion object {
        const val TAG = "ProductListFragment"
        const val NUMBER_OF_COLUMNS = 2
        fun newInstance(): ProductListFragment = ProductListFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_products_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerView()
        setEmptyView()
        setRefreshBehaviour()
        setScrollListener()
        productListPresenter.start()
    }

    private fun setScrollListener() {
        productsListRV.addOnScrollListener(onScrollListener)
    }

    private fun setRefreshBehaviour() {
        swipeRefreshLayout.setOnRefreshListener {
            productListPresenter.start()
            (productsListRV.adapter as? ProductListAdapter)?.restartLastPosition()
        }
    }

    private fun setEmptyView() {
        emptyView.fillViews(EmptyViewEnumeration.MOVIES_LIST_EMPTY_VIEW)
    }

    override fun onResume() {
        super.onResume()
        productListPresenter.resume()
    }

    private fun setRecyclerView() {
        productsListRV.layoutManager = gridLayoutManager
    }

    override fun setupFragmentComponent() {
        ProximityProductsListApplication
                .applicationComponent
                .plus(BaseFragmentModule(context!!), ProductListModule(this))
                .inject(this)
    }

    override fun onPause() {
        super.onPause()
        productListPresenter.pause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        productsListRV.removeOnScrollListener(onScrollListener)
        productListPresenter.destroy()
    }

    override fun setItems(productList: List<BaseListItem>) {
        productsListRV?.let {
            if (productsListRV.adapter == null) productsListRV.adapter = ProductListAdapter(productList, this)
            productsListRV.adapter.notifyDataSetChanged()
        }
    }

    override fun showEmptyView() {
        emptyView.visibility = View.VISIBLE
    }

    override fun hideRecyclerView() {
        productsListRV.visibility = View.GONE
    }

    override fun showProgressBar(showProgress: Boolean) {
        if (!swipeRefreshLayout.isRefreshing) {
            progressBar.visibility = if (showProgress) View.VISIBLE else View.GONE
        } else swipeRefreshLayout.isRefreshing = showProgress
    }

    override fun showRecyclerView() {
        productsListRV.visibility = View.VISIBLE
    }

    override fun hideEmptyView() {
        emptyView.visibility = View.GONE
    }

    override fun provideContext(): Context? = context

    override fun showDistanceRangeView() {
        distanceRangeView.visibility = View.VISIBLE
    }

    override fun setDistanceRange(initialDistance: Int, finalDistance: Int) {
        distanceRangeView.setRange(initialDistance, finalDistance)
    }

    override fun hideDistanceRangeView() {
        distanceRangeView.visibility = View.GONE
    }

    override fun onItemClicked(position: Int, view: View) {
        productListPresenter.handleOnItemClicked(position, view)
    }
}