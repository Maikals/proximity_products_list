package com.example.miquelcastanys.proximityproductslist.views.productDetail

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.transition.Slide
import com.example.miquelcastanys.proximityproductslist.R
import com.example.miquelcastanys.proximityproductslist.entities.enumerations.ProductTypeEnum
import com.example.miquelcastanys.proximityproductslist.extensions.loadImageFromUrl
import com.example.miquelcastanys.proximityproductslist.interfaces.DetailActivityFragmentCommunicationInterface
import com.example.miquelcastanys.proximityproductslist.views.baseViews.BaseActivity
import com.example.miquelcastanys.proximityproductslist.views.baseViews.BaseFragment
import kotlinx.android.synthetic.main.activity_product_detail.*

class ProductDetailActivity : BaseActivity(), DetailActivityFragmentCommunicationInterface {

    companion object {
        const val TAG = "ProductDetailActivity"
        private const val EXTRA_PRODUCT_ID = "ExtraProductId"

        fun newIntent(context: Context, productId: String): Intent {
            return Intent(context, ProductDetailActivity::class.java).apply {
                putExtra(EXTRA_PRODUCT_ID, productId)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) initActivityTransitions()
        setToolbar()
        setToolbarBackButton()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun initActivityTransitions() {
        val transition = Slide()
        transition.excludeTarget(android.R.id.statusBarBackground, true)
        window.enterTransition = transition
        window.returnTransition = transition
    }

    override fun getLayout(): Int = R.layout.activity_product_detail

    override fun createFragmentAndTag(): Pair<BaseFragment, String> =
            ProductDetailFragment.newInstance(intent.getStringExtra(EXTRA_PRODUCT_ID)) to
                    ProductDetailFragment.TAG

    override fun setParameters(name: String, imageUrl: String, typeEnum: ProductTypeEnum) {
        headerProductImage.loadImageFromUrl(imageUrl, typeEnum)
        collapsingToolbar.title = name
    }
}
