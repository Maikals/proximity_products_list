package com.example.miquelcastanys.proximityproductslist.entities

import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseProductListItem

class AdvertisementProductListItem(image: String,
                                   name: String,
                                   val advertisementUrl: String) : BaseProductListItem("", image, "", name, "", "")