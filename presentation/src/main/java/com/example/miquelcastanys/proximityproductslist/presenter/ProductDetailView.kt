package com.example.miquelcastanys.proximityproductslist.presenter

import android.content.Context
import com.example.miquelcastanys.proximityproductslist.entities.ProductDetail


interface ProductDetailView {

    fun hideEmptyView()

    fun showEmptyView()

    fun showProductInfo()

    fun hideProductInfo()

    fun setData(productDetail: ProductDetail)

    fun provideContext(): Context?
}