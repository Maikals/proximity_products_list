package com.example.miquelcastanys.proximityproductslist.views.productsList

import android.content.Context
import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseListItem


interface ProductListView {
    fun setItems(productList: List<BaseListItem>)
    fun showEmptyView()
    fun hideRecyclerView()
    fun showProgressBar(showProgress: Boolean)
    fun showRecyclerView()
    fun hideEmptyView()
    fun provideContext(): Context?
    fun setDistanceRange(initialDistance: Int, finalDistance: Int)
    fun showDistanceRangeView()
    fun hideDistanceRangeView()
}