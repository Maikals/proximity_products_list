package com.example.miquelcastanys.proximityproductslist.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.example.miquelcastanys.proximityproductslist.R
import com.example.miquelcastanys.proximityproductslist.entities.AdvertisementProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.CarProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.ConsumerGoodsProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.ServiceProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseListItem
import com.example.miquelcastanys.proximityproductslist.entities.enumerations.ProductTypeEnum
import com.example.miquelcastanys.proximityproductslist.extensions.inflateFromLayout
import com.example.miquelcastanys.proximityproductslist.interfaces.OnListItemClickInterface
import com.example.miquelcastanys.proximityproductslist.utils.PresentationConstants
import com.example.miquelcastanys.proximityproductslist.views.viewHolders.*
import com.example.miquelcastanys.proximityproductslist.views.viewHolders.baseViewHolders.BaseViewHolder


class ProductListAdapter(private val productList: List<BaseListItem>, private val listener: OnListItemClickInterface.ViewSide)
    : RecyclerView.Adapter<BaseViewHolder>(), OnListItemClickInterface.AdapterSide {

    override fun onItemClicked(position: Int, view: View) {
        listener.onItemClicked(position, view)
    }

    private var lastPosition: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder =
            when (viewType) {
                ProductTypeEnum.CAR.ordinal -> CarProductViewHolder(parent.inflateFromLayout(R.layout.list_item_car), this)
                ProductTypeEnum.SERVICE.ordinal -> ServiceProductViewHolder(parent.inflateFromLayout(R.layout.list_item_service), this)
                ProductTypeEnum.CONSUMER_GOOD.ordinal -> ConsumerGoodProductViewHolder(parent.inflateFromLayout(R.layout.list_item_consumer_good), this)
                ProductTypeEnum.ADVERTISEMENT.ordinal -> AdvertisementProductViewHolder(parent.inflateFromLayout(R.layout.list_item_advertisement), this)
                else -> FooterViewHolder(parent.inflateFromLayout(R.layout.list_item_footer))
            }

    override fun getItemCount(): Int = productList.size

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        when (holder) {
            is CarProductViewHolder -> holder.bindView(productList[position] as CarProductListItem)
            is ConsumerGoodProductViewHolder -> holder.bindView(productList[position] as ConsumerGoodsProductListItem)
            is ServiceProductViewHolder -> holder.bindView(productList[position] as ServiceProductListItem)
            is AdvertisementProductViewHolder-> holder.bindView(productList[position] as AdvertisementProductListItem)
        }
        setAnimation(holder.view, position)
    }

    override fun getItemViewType(position: Int): Int =
            when (productList[position]) {
                is CarProductListItem -> ProductTypeEnum.CAR.ordinal
                is ConsumerGoodsProductListItem -> ProductTypeEnum.CONSUMER_GOOD.ordinal
                is ServiceProductListItem -> ProductTypeEnum.SERVICE.ordinal
                is AdvertisementProductListItem -> ProductTypeEnum.ADVERTISEMENT.ordinal
                else -> PresentationConstants.ITEM_TYPE_FOOTER
            }

    fun restartLastPosition() {
        lastPosition = -1
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(viewToAnimate.context, R.anim.slide_in_bottom)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    override fun onViewRecycled(holder: BaseViewHolder) {
        super.onViewRecycled(holder)
        holder.clearAnimation()
    }
}