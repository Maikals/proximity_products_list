package com.example.miquelcastanys.proximityproductslist.entities.enumerations

import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import com.example.miquelcastanys.proximityproductslist.R

enum class EmptyViewEnumeration(@DrawableRes val imageId: Int, @StringRes val title: Int, @StringRes val subtitle: Int) {
    MOVIES_LIST_EMPTY_VIEW(R.drawable.ic_empty_view, R.string.error_title_no_products, R.string.error_description_no_products),
    MOVIES_DETAIL_EMPTY_VIEW(0, R.string.error_title_no_detail, R.string.error_description_no_detail)
}