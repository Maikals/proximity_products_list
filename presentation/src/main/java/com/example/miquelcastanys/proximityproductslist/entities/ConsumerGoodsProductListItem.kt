package com.example.miquelcastanys.proximityproductslist.entities

import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseProductListItem


class ConsumerGoodsProductListItem(id: String,
                                   image: String,
                                   price: String,
                                   name: String,
                                   val color: String,
                                   val category: String,
                                   description: String,
                                   distanceInMeters: String) : BaseProductListItem(id, image, price, name, description, distanceInMeters) {
}