package com.example.miquelcastanys.proximityproductslist

import android.app.Application
import com.example.miquelcastanys.proximityproductslist.injector.component.ApplicationComponent
import com.example.miquelcastanys.proximityproductslist.injector.component.DaggerApplicationComponent
import com.example.miquelcastanys.proximityproductslist.injector.modules.ApplicationModule
import com.squareup.leakcanary.LeakCanary

class ProximityProductsListApplication : Application() {

    companion object {
        lateinit var instance: ProximityProductsListApplication
        @JvmStatic
        lateinit var applicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        applicationComponent = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init our app in this process.
            return
        }
        LeakCanary.install(this)
    }

}