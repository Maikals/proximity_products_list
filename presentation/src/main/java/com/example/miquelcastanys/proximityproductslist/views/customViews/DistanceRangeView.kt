package com.example.miquelcastanys.proximityproductslist.views.customViews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import com.example.miquelcastanys.proximityproductslist.R
import kotlinx.android.synthetic.main.distance_range_view.view.*

class DistanceRangeView : FrameLayout {
    private var rangeText: TextView? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    private fun init() {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.distance_range_view, this, true)
        rangeText = view.rangeText
    }

    fun setRange(initialDistance: Int, finalDistance: Int) {
        rangeText?.text = createDistanceText(initialDistance, finalDistance)
    }

    private fun createDistanceText(initialDistance: Int, finalDistance: Int) =
            "${context.getString(R.string.showing_products)} " +
                    "$initialDistance ${context.getString(R.string.meters)} " +
                    "${context.getString(R.string.to)} " +
                    "$finalDistance ${context.getString(R.string.meters)} " +
                    context.getString(R.string.from_you)
}