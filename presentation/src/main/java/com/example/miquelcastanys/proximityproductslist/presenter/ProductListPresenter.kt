package com.example.miquelcastanys.proximityproductslist.presenter

import android.content.Context
import android.view.View
import com.example.domain.entities.baseEntities.BaseProduct
import com.example.domain.interactor.GetProductListUseCase
import com.example.miquelcastanys.proximityproductslist.entities.AdvertisementProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.ProductObserver
import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseListItem
import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.mappers.ProductListMapper
import com.example.miquelcastanys.proximityproductslist.injector.PerFragment
import com.example.miquelcastanys.proximityproductslist.interfaces.ObserverPresenterInterface
import com.example.miquelcastanys.proximityproductslist.navigation.Navigator
import com.example.miquelcastanys.proximityproductslist.views.productsList.ProductListView
import javax.inject.Inject


@PerFragment
class ProductListPresenter @Inject constructor(private val getProductListUseCase: GetProductListUseCase) : Presenter, ObserverPresenterInterface<List<BaseProduct>> {

    companion object {
        const val TAG = "ProductListPresenter"
    }

    @Inject
    lateinit var view: ProductListView
    @Inject
    lateinit var navigator: Navigator
    var productList: ArrayList<BaseListItem> = ArrayList()

    fun start() {
        getProximityProductList(true)
    }

    fun getProximityProductList(refreshList: Boolean) {
        view.showProgressBar(true)
        getProductListUseCase.execute(null, ProductObserver(this, refreshList))
    }

    private fun addResultToProductList(moviesListResult: List<BaseListItem>) {
        productList.addAll(moviesListResult)
    }

    override fun onNext(result: List<BaseProduct>, refreshList: Boolean) {
        if (result.isNotEmpty()) manageResult(refreshList, result)
        else manageEmptyResult()
    }

    private fun manageResult(refreshList: Boolean, result: List<BaseProduct>) {
        if (refreshList) productList.clear()
        view.provideContext()?.let {
            addResultToProductList(ProductListMapper.convertTo(result, view.provideContext() as Context))
        }
        view.hideEmptyView()
        view.showRecyclerView()
        view.setItems(productList)
        view.showProgressBar(false)
    }

    override fun onComplete() {

    }

    override fun onError(e: Throwable) {
        manageEmptyResult()
    }

    private fun manageEmptyResult() {
        productList.clear()
        view.hideRecyclerView()
        view.showEmptyView()
        view.hideDistanceRangeView()
        view.showProgressBar(false)
    }

    override fun resume() {
    }

    override fun pause() {
    }

    override fun destroy() {
        getProductListUseCase.dispose()
    }

    private fun openProductDetail(position: Int, view: View) {
        this.view.hideDistanceRangeView()
        if (productList[position] is BaseProductListItem && this.view.provideContext() != null) {
            navigator.toProductDetail(this.view.provideContext()!!, (productList[position] as BaseProductListItem).id, view)
        }
    }

    fun calculateDistanceRange(findFirstVisibleItemPositions: IntArray, findLastVisibleItemPositions: IntArray) {
        val first = findFirstVisibleItemPositions.min()
        val last = findLastVisibleItemPositions.max()
        if ((productList[first!!] as BaseProductListItem).distanceInMeters != "" &&
                (productList[last!!] as BaseProductListItem).distanceInMeters != "") {
            view.setDistanceRange((productList[first] as BaseProductListItem).distanceInMeters.removeSuffix("m").toInt(),
                    (productList[last] as BaseProductListItem).distanceInMeters.removeSuffix("m").toInt())
            view.showDistanceRangeView()
        }
    }

    fun handleOnItemClicked(position: Int, view: View) {
        if (productList[position] is AdvertisementProductListItem) {
            openUrlBrowser((productList[position] as AdvertisementProductListItem).advertisementUrl)
        } else {
            openProductDetail(position, view)
        }
    }

    private fun openUrlBrowser(url: String) {
        navigator.toBrowser(view.provideContext()!!, url)
    }
}
