package com.example.miquelcastanys.proximityproductslist.interfaces


interface ObserverPresenterInterface<T> {

    fun onNext(result: T, refreshList: Boolean = false)

    fun onComplete()

    fun onError(e: Throwable)
}