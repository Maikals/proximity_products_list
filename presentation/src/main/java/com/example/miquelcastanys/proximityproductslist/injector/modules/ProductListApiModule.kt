package com.example.miquelcastanys.proximityproductslist.injector.modules

import com.example.data.net.ApiConstants
import com.example.data.net.ProductListService
import com.example.data.net.interceptor.RequestInterceptor
import com.example.data.repository.ProductListRepositoryImpl
import com.example.data.repository.dataSource.ProductListDataStore
import com.example.data.repository.dataSource.ProductListDataStoreImpl
import com.example.domain.repository.ProductListRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ProductListApiModule {

    @Provides
    @Singleton
    fun provideProductListDataStore(productListDataStoreImpl: ProductListDataStoreImpl): ProductListDataStore = productListDataStoreImpl

    @Provides
    @Singleton
    fun provideProductListRepository(productListRepositoryImpl: ProductListRepositoryImpl): ProductListRepository = productListRepositoryImpl

    @Provides
    @Singleton
    fun productListApi(interceptor: RequestInterceptor): ProductListService {
        val httpClient = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(ApiConstants.TIMEOUT_CONNECTION_VALUE, TimeUnit.SECONDS)
                .readTimeout(ApiConstants.TIMEOUT_READ_VALUE, TimeUnit.SECONDS)
                .writeTimeout(ApiConstants.TIMEOUT_WRITE_VALUE, TimeUnit.SECONDS)
        val builder = Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(
                        RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
        return builder.client(httpClient.build()).build().create(ProductListService::class.java)
    }
}