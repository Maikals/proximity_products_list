package com.example.miquelcastanys.proximityproductslist.views.viewHolders.baseViewHolders

import android.view.View
import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.enumerations.ProductTypeEnum
import com.example.miquelcastanys.proximityproductslist.extensions.loadImageFromUrl
import com.example.miquelcastanys.proximityproductslist.interfaces.OnListItemClickInterface
import kotlinx.android.synthetic.main.list_item_car.view.*


abstract class BaseProductViewHolder(view: View, private val listener: OnListItemClickInterface.AdapterSide)
    : BaseViewHolder(view) {

    init {
        view.setOnClickListener{ listener.onItemClicked(adapterPosition, view.productImage) }
    }

    fun bindView(baseProductListItem: BaseProductListItem) {
        view.productImage.loadImageFromUrl(baseProductListItem.image,
                ProductTypeEnum.getFromInstance(baseProductListItem))
        view.productName.text = baseProductListItem.name
        if (view.productPrice != null) {
            view.productPrice.text = baseProductListItem.price
        }
    }
}