package com.example.miquelcastanys.proximityproductslist.injector.modules

import com.example.miquelcastanys.proximityproductslist.injector.PerFragment
import com.example.miquelcastanys.proximityproductslist.views.productsList.ProductListFragment
import com.example.miquelcastanys.proximityproductslist.views.productsList.ProductListView
import dagger.Module
import dagger.Provides

@Module
class ProductListModule(private val productListFragment: ProductListFragment) {

    @Provides
    @PerFragment
    fun provideProductListView(): ProductListView {
        return productListFragment
    }
}