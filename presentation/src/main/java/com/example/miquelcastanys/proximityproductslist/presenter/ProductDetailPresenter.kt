package com.example.miquelcastanys.proximityproductslist.presenter

import android.content.Context
import com.example.domain.entities.baseEntities.BaseProduct
import com.example.domain.entities.params.ProductDetailParams
import com.example.domain.interactor.GetProductDetailUseCase
import com.example.miquelcastanys.proximityproductslist.entities.ProductObserver
import com.example.miquelcastanys.proximityproductslist.entities.mappers.ProductDetailMapper
import com.example.miquelcastanys.proximityproductslist.injector.PerFragment
import com.example.miquelcastanys.proximityproductslist.interfaces.ObserverPresenterInterface
import javax.inject.Inject

@PerFragment
class ProductDetailPresenter @Inject constructor(private val getProductDetailUseCase: GetProductDetailUseCase) : Presenter, ObserverPresenterInterface<BaseProduct> {

    @Inject
    lateinit var view: ProductDetailView

    override fun resume() {

    }

    override fun pause() {

    }

    override fun destroy() {
        getProductDetailUseCase.dispose()
    }

    fun start(productId: String) {
        getProductDetailUseCase.execute(ProductDetailParams(productId), ProductObserver(this))
    }

    override fun onNext(result: BaseProduct, refreshList: Boolean) {
        view.provideContext()?.let {
            view.setData(ProductDetailMapper.convertTo(result, view.provideContext() as Context))
            view.hideEmptyView()
            view.showProductInfo()
        }
    }

    override fun onComplete() {

    }

    override fun onError(e: Throwable) {
        view.hideProductInfo()
        view.showEmptyView()
    }

}