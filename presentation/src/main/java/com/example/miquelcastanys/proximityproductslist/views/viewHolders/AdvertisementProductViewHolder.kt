package com.example.miquelcastanys.proximityproductslist.views.viewHolders

import android.view.View
import com.example.miquelcastanys.proximityproductslist.entities.AdvertisementProductListItem
import com.example.miquelcastanys.proximityproductslist.interfaces.OnListItemClickInterface
import com.example.miquelcastanys.proximityproductslist.views.viewHolders.baseViewHolders.BaseProductViewHolder
import kotlinx.android.synthetic.main.list_item_car.view.*


class AdvertisementProductViewHolder(view: View, listener: OnListItemClickInterface.AdapterSide)
    : BaseProductViewHolder(view, listener) {
    fun bindView(advertisementProductListItem: AdvertisementProductListItem) {
        super.bindView(advertisementProductListItem)
        view.productName.text = advertisementProductListItem.name
    }
}