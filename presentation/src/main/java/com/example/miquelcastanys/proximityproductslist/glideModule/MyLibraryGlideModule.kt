package com.example.miquelcastanys.proximityproductslist.glideModule

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.LibraryGlideModule

@GlideModule
class MyLibraryGlideModule : LibraryGlideModule()