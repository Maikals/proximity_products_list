package com.example.miquelcastanys.proximityproductslist.entities.mappers

import android.content.Context
import com.example.domain.entities.AdvertisementProduct
import com.example.domain.entities.CarProduct
import com.example.domain.entities.ConsumerGoodProduct
import com.example.domain.entities.ServiceProduct
import com.example.domain.entities.baseEntities.BaseProduct
import com.example.miquelcastanys.proximityproductslist.R
import com.example.miquelcastanys.proximityproductslist.entities.AdvertisementProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.CarProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.ConsumerGoodsProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.ServiceProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseListItem

object ProductListMapper {

    fun convertTo(list: List<BaseProduct>, context: Context): List<BaseListItem> =
            list.map { convertTo(it, context) }

    private fun convertTo(product: BaseProduct, context: Context): BaseListItem =
            when (product) {
                is CarProduct -> createFromCarProduct(product, context)
                is ConsumerGoodProduct -> createFromConsumerGoodProduct(product, context)
                is ServiceProduct -> createFromServiceProduct(product, context)
                else -> createFromAdvertisementProduct(product as AdvertisementProduct)
            }

    private fun createFromAdvertisementProduct(advertisementProduct: AdvertisementProduct): AdvertisementProductListItem =
            AdvertisementProductListItem(advertisementProduct.image, advertisementProduct.name, advertisementProduct.advertisementUrl)


    private fun createFromServiceProduct(product: ServiceProduct, context: Context) =
            ServiceProductListItem(product.id,
                    product.image,
                    product.price,
                    product.name,
                    product.closeDay,
                    product.category,
                    "${context.getString(R.string.minimum_age)}: ${product.minimumAge} ${context.getString(R.string.years)}" ,
                    product.description,
                    "${product.distanceInMeters}${context.getString(R.string.meters)}")

    private fun createFromConsumerGoodProduct(product: ConsumerGoodProduct, context: Context) =
            ConsumerGoodsProductListItem(product.id,
                    product.image,
                    product.price,
                    product.name,
                    product.color,
                    product.category,
                    product.description,
                    "${product.distanceInMeters}${context.getString(R.string.meters)}")


    private fun createFromCarProduct(product: CarProduct, context: Context) =
            CarProductListItem(product.id,
                    product.image,
                    product.price,
                    product.name,
                    product.motor,
                    product.gearbox,
                    product.brand,
                    "${product.km} ${context.getString(R.string.kilometers)}",
                    product.description,
                    "${product.distanceInMeters}${context.getString(R.string.meters)}")
}