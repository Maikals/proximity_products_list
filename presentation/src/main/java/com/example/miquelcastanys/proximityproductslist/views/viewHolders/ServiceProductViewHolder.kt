package com.example.miquelcastanys.proximityproductslist.views.viewHolders

import android.view.View
import com.example.miquelcastanys.proximityproductslist.entities.ServiceProductListItem
import com.example.miquelcastanys.proximityproductslist.interfaces.OnListItemClickInterface
import com.example.miquelcastanys.proximityproductslist.views.viewHolders.baseViewHolders.BaseProductViewHolder
import kotlinx.android.synthetic.main.list_item_service.view.*


class ServiceProductViewHolder(view: View, listener: OnListItemClickInterface.AdapterSide)
    : BaseProductViewHolder(view, listener) {

    fun bindView(serviceProductListItem: ServiceProductListItem) {
        super.bindView(serviceProductListItem)
        view.serviceCategory.text = serviceProductListItem.category
        view.serviceDescription.text = serviceProductListItem.description
        view.serviceCloseDay.text = serviceProductListItem.closeDay
        view.serviceMinimumAge.text = serviceProductListItem.minimumAge
    }
}