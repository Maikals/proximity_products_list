package com.example.miquelcastanys.proximityproductslist.views.viewHolders

import android.view.View
import com.example.miquelcastanys.proximityproductslist.interfaces.OnListItemClickInterface
import com.example.miquelcastanys.proximityproductslist.views.viewHolders.baseViewHolders.BaseViewHolder

class FooterViewHolder(view: View)
    : BaseViewHolder(view)