package com.example.miquelcastanys.proximityproductslist.interfaces

import android.view.View


interface OnListItemClickInterface {
    interface AdapterSide {
        fun onItemClicked(position: Int, view: View)
    }

    interface ViewSide {
        fun onItemClicked(position: Int, view: View)
    }

}