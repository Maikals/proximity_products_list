package com.example.miquelcastanys.proximityproductslist.injector.modules

import android.content.Context
import com.example.domain.executor.PostExecutionThread
import com.example.miquelcastanys.proximityproductslist.ProximityProductsListApplication
import com.example.miquelcastanys.proximityproductslist.UIThread
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: ProximityProductsListApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideUIThread(uiThread: UIThread): PostExecutionThread {
        return uiThread
    }

}