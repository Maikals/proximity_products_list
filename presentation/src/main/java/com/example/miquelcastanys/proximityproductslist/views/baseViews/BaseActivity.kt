package com.example.miquelcastanys.proximityproductslist.views.baseViews

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.miquelcastanys.proximityproductslist.R
import kotlinx.android.synthetic.main.activity_product_detail.*


abstract class BaseActivity : AppCompatActivity() {

    private lateinit var currentFragment: BaseFragment
    private lateinit var currentFragmentTag: String

    companion object {
        private const val EXTRA_FRAGMENT = "extra_fragment"
        private const val EXTRA_FRAGMENT_TAG = "extra_fragment_tag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        setCurrentFragment(savedInstanceState)
        showFragment()
    }

    private fun showFragment() =
            supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, currentFragment, currentFragmentTag).commit()

    private fun setCurrentFragment(savedInstanceState: Bundle?) =
            if (savedInstanceState != null
                    && savedInstanceState.containsKey(EXTRA_FRAGMENT)
                    && savedInstanceState.containsKey(EXTRA_FRAGMENT)) {
                currentFragmentTag = savedInstanceState.getString(EXTRA_FRAGMENT_TAG)
                currentFragment = supportFragmentManager.getFragment(savedInstanceState, currentFragmentTag) as BaseFragment
            } else
                setCurrentFragmentAndTag(createFragmentAndTag())

    override fun onSaveInstanceState(outState: Bundle?) {
        supportFragmentManager.putFragment(outState, currentFragmentTag, currentFragment)
        outState?.putString(EXTRA_FRAGMENT_TAG, currentFragmentTag)
        super.onSaveInstanceState(outState)
    }

    abstract fun getLayout(): Int

    abstract fun createFragmentAndTag(): Pair<BaseFragment, String>

    private fun setCurrentFragmentAndTag(pair: Pair<BaseFragment, String>) {
        currentFragment = pair.first
        currentFragmentTag = pair.second
    }

    protected fun setToolbar() {
        setSupportActionBar(toolbar)
    }

    protected fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    protected fun setToolbarBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}