package com.example.miquelcastanys.proximityproductslist.injector.modules

import android.content.Context
import dagger.Module

@Module
class BaseFragmentModule(private val context: Context)