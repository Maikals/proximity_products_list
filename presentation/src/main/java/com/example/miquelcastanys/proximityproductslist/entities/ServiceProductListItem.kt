package com.example.miquelcastanys.proximityproductslist.entities

import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseProductListItem

class ServiceProductListItem(id: String,
                             image: String,
                             price: String,
                             name: String,
                             val closeDay: String,
                             val category: String,
                             val minimumAge: String,
                             description: String,
                             distanceInMeters: String) : BaseProductListItem(id, image, price, name, description, distanceInMeters)