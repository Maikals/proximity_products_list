package com.example.miquelcastanys.proximityproductslist.injector.component

import com.example.miquelcastanys.proximityproductslist.injector.PerFragment
import com.example.miquelcastanys.proximityproductslist.injector.modules.BaseFragmentModule
import com.example.miquelcastanys.proximityproductslist.injector.modules.ProductDetailModule
import com.example.miquelcastanys.proximityproductslist.views.productDetail.ProductDetailFragment
import dagger.Subcomponent

@PerFragment
@Subcomponent(modules = [BaseFragmentModule::class, ProductDetailModule::class])
interface ProductDetailComponent {

    fun inject(productDetailFragment: ProductDetailFragment)

}