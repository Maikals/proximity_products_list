package com.example.miquelcastanys.proximityproductslist.views.productsList

import com.example.miquelcastanys.proximityproductslist.R
import com.example.miquelcastanys.proximityproductslist.views.baseViews.BaseActivity
import com.example.miquelcastanys.proximityproductslist.views.baseViews.BaseFragment

class ProductsListActivity : BaseActivity() {

    override fun getLayout(): Int = R.layout.activity_products_list

    override fun createFragmentAndTag(): Pair<BaseFragment, String> =
            ProductListFragment.newInstance() to ProductListFragment.TAG
}
