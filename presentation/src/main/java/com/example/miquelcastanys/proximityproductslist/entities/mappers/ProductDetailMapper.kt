package com.example.miquelcastanys.proximityproductslist.entities.mappers

import android.content.Context
import com.example.domain.entities.CarProduct
import com.example.domain.entities.ConsumerGoodProduct
import com.example.domain.entities.ServiceProduct
import com.example.domain.entities.baseEntities.BaseProduct
import com.example.miquelcastanys.proximityproductslist.R
import com.example.miquelcastanys.proximityproductslist.entities.ProductDetail
import com.example.miquelcastanys.proximityproductslist.entities.enumerations.ProductTypeEnum

object ProductDetailMapper {
    fun convertTo(product: BaseProduct, context: Context): ProductDetail =
            when (product) {
                is CarProduct -> createCarDetail(product, context)
                is ConsumerGoodProduct -> createConsumerGoodDetail(product, context)
                else -> createServiceDetail(product as ServiceProduct, context)
            }

    private fun createServiceDetail(product: ServiceProduct, context: Context): ProductDetail =
            ProductDetail(product.name,
                    product.image,
                    product.price,
                    product.description,
                    createDistanceField(context, product.distanceInMeters),
                    createServiceFields(product, context),
                    ProductTypeEnum.SERVICE)

    private fun createServiceFields(product: ServiceProduct, context: Context): List<String> =
            listOf("${context.getString(R.string.category)}: ${product.category}",
                    "${context.getString(R.string.close_day)}: ${product.closeDay}",
                    "${context.getString(R.string.minimum_age)}: ${product.minimumAge}")

    private fun createConsumerGoodDetail(product: ConsumerGoodProduct, context: Context): ProductDetail =
            ProductDetail(product.name,
                    product.image,
                    product.price,
                    product.description,
                    createDistanceField(context, product.distanceInMeters),
                    createConsumerGoodFields(product, context),
                    ProductTypeEnum.CONSUMER_GOOD)

    private fun createConsumerGoodFields(product: ConsumerGoodProduct, context: Context): List<String> =
            listOf("${context.getString(R.string.category)}: ${product.category}",
                    "${context.getString(R.string.color)}: ${product.color}")


    private fun createCarDetail(product: CarProduct, context: Context) =
            ProductDetail(product.name,
                    product.image,
                    product.price,
                    product.description,
                    createDistanceField(context, product.distanceInMeters),
                    createCarFields(product, context),
                    ProductTypeEnum.CAR)

    private fun createCarFields(product: CarProduct, context: Context): List<String> =
            listOf("${context.getString(R.string.brand)}: ${product.brand}",
                    "${context.getString(R.string.gearbox)}: ${product.gearbox}",
                    "${context.getString(R.string.motor)}: ${product.motor}",
                    "${context.getString(R.string.mileage)}: ${product.km} ${context.getString(R.string.kilometers)}")

    private fun createDistanceField(context: Context, distance: Int) =
            "${context.getString(R.string.distance)}: $distance${context.getString(R.string.meters)}"
}
