package com.example.miquelcastanys.proximityproductslist.entities

import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseProductListItem


class CarProductListItem(id: String,
                         image: String,
                         price: String,
                         name: String,
                         val motor: String,
                         val gearbox: String,
                         val brand: String,
                         val km: String,
                         description: String,
                         distanceInMeters: String) : BaseProductListItem(id, image, price, name, description, distanceInMeters)