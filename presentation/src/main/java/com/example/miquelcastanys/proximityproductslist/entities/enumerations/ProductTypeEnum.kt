package com.example.miquelcastanys.proximityproductslist.entities.enumerations

import android.support.annotation.DrawableRes
import com.example.miquelcastanys.proximityproductslist.R
import com.example.miquelcastanys.proximityproductslist.entities.CarProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.ConsumerGoodsProductListItem
import com.example.miquelcastanys.proximityproductslist.entities.baseEntities.BaseProductListItem


enum class ProductTypeEnum(@DrawableRes val drawableId: Int) {
    CAR(R.drawable.ic_car_placeholder),
    CONSUMER_GOOD(R.drawable.ic_consumer_goods_placeholder),
    SERVICE(R.drawable.ic_service_placeholder),
    ADVERTISEMENT(R.drawable.ic_advertisement_placeholder);

    companion object {

        fun getFromInstance(baseProductListItem: BaseProductListItem) =
                when (baseProductListItem) {
                    is CarProductListItem -> CAR
                    is ConsumerGoodsProductListItem -> CONSUMER_GOOD
                    else -> SERVICE
                }
    }
}