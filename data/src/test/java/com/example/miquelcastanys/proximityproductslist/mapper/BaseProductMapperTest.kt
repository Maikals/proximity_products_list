package com.example.miquelcastanys.proximityproductslist.mapper

import com.example.data.entities.ProductListDTO
import com.example.data.entities.ProductListItemDTO
import com.example.data.entities.mappers.base.BaseProductMapper
import com.example.domain.entities.CarProduct
import com.example.domain.entities.ConsumerGoodProduct
import com.example.domain.entities.ServiceProduct
import com.example.miquelcastanys.proximityproductslist.UnitTest
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Test

class BaseProductMapperTest : UnitTest() {
    companion object {
        private const val CAR_ID = "1"
        private const val CONSUMER_GOOD_ID = "2"
        private const val SERVICE_ID = "3"

        private const val CAR_TYPE = "car"
        private const val CONSUMER_GOOD_TYPE = "consumer_goods"
        private const val SERVICE_TYPE = "service"
    }

    private val carProduct = createProductListDTO(CAR_ID, CAR_TYPE)
    private val consumerGoodProduct = createProductListDTO(CONSUMER_GOOD_ID, CONSUMER_GOOD_TYPE)
    private val serviceProduct = createProductListDTO(SERVICE_ID, SERVICE_TYPE)


    @Test
    fun createSpecificProductFromBaseTest() {

        val resultCar = BaseProductMapper.createSpecificProductFromBase(carProduct)

        Assert.assertThat(resultCar,
                CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(CarProduct::class.java)))

        val resultConsumerGood = BaseProductMapper.createSpecificProductFromBase(consumerGoodProduct)

        Assert.assertThat(resultConsumerGood,
                CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(ConsumerGoodProduct::class.java)))

        val resultService = BaseProductMapper.createSpecificProductFromBase(serviceProduct)

        Assert.assertThat(resultService,
                CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(ServiceProduct::class.java)))
    }

    @Test
    fun createCarProductTest() {
        val resultCar = BaseProductMapper.createCarProduct(carProduct.item!!)

        Assert.assertThat(resultCar,
                CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(CarProduct::class.java)))
    }

    @Test
    fun createConsumerGoodProductTest() {
        val resultConsumerGood = BaseProductMapper.createConsumerGoodsProduct(consumerGoodProduct.item!!)

        Assert.assertThat(resultConsumerGood,
                CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(ConsumerGoodProduct::class.java)))
    }

    @Test
    fun createServiceProductTest() {
        val resultService = BaseProductMapper.createServiceProduct(serviceProduct.item!!)

        Assert.assertThat(resultService,
                CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(ServiceProduct::class.java)))
    }


    private fun createProductListDTO(id: String, type: String) = ProductListDTO(type, createProductListItemDTO(id))

    private fun createProductListItemDTO(id: String) =
            ProductListItemDTO(id,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    0,
                    "",
                    "",
                    "",
                    0,
                    "",
                    0)
}