package com.example.miquelcastanys.proximityproductslist.dataStore

import com.example.data.entities.ProductListDTO
import com.example.data.entities.ProductListItemDTO
import com.example.data.net.ProductListService
import com.example.data.repository.dataSource.ProductDetailDataStoreImpl
import com.example.domain.entities.CarProduct
import com.example.domain.entities.ConsumerGoodProduct
import com.example.domain.entities.ServiceProduct
import com.example.miquelcastanys.proximityproductslist.UnitTest
import com.nhaarman.mockito_kotlin.given
import io.reactivex.Single
import io.reactivex.SingleObserver
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock


class ProductDetailDataStoreImplTest : UnitTest() {

    private lateinit var productDetailDataStoreImpl: ProductDetailDataStoreImpl
    @Mock
    private lateinit var productListService: ProductListService

    @Before
    fun setUp() {
        productDetailDataStoreImpl = ProductDetailDataStoreImpl(productListService)
    }

    @Test
    fun getProductDetailTest() {
        given(productListService.getProductList()).willReturn(SingleList())

        val productListCarObservable = productDetailDataStoreImpl.getProductDetail("1")
        val productDetailCar = productListCarObservable.blockingGet()

        assertThat(productDetailCar, `is`<Any>(instanceOf<Any>(CarProduct::class.java)))

        val productListConsumerGoodsObservable = productDetailDataStoreImpl.getProductDetail("2")
        val productDetailConsumerGoods = productListConsumerGoodsObservable.blockingGet()

        assertThat(productDetailConsumerGoods, `is`<Any>(instanceOf<Any>(ConsumerGoodProduct::class.java)))

        val productListServiceObservable = productDetailDataStoreImpl.getProductDetail("3")
        val productDetailService = productListServiceObservable.blockingGet()
        assertThat(productDetailService, `is`<Any>(instanceOf<Any>(ServiceProduct::class.java)))
    }

    class SingleList : Single<List<ProductListDTO>>() {
        override fun subscribeActual(observer: SingleObserver<in List<ProductListDTO>>) {
            observer.onSuccess(createProductList())
        }

        private fun createProductList(): List<ProductListDTO> = listOf(createProductListDTO("car", "1"),
                createProductListDTO("consumer_goods", "2"),
                createProductListDTO("service", "3"))

        private fun createProductListDTO(kind: String, id: String) =
                ProductListDTO(kind, ProductListItemDTO(id, "", "", "", "", "", "", 0, "", "", "", 0, "", 0))

    }
}