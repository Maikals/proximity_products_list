package com.example.miquelcastanys.proximityproductslist.dataStore

import com.example.data.entities.ProductListDTO
import com.example.data.entities.ProductListItemDTO
import com.example.data.net.ProductListService
import com.example.data.repository.dataSource.ProductListDataStoreImpl
import com.example.domain.entities.CarProduct
import com.example.domain.entities.ConsumerGoodProduct
import com.example.domain.entities.ServiceProduct
import com.example.miquelcastanys.proximityproductslist.UnitTest
import com.nhaarman.mockito_kotlin.given
import io.reactivex.Single
import io.reactivex.SingleObserver
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock


class ProductListDataStoreImplTest : UnitTest() {

    private lateinit var productListDataStoreImpl: ProductListDataStoreImpl
    @Mock
    private lateinit var productListService: ProductListService

    @Before
    fun setUp() {
        productListDataStoreImpl = ProductListDataStoreImpl(productListService)
    }

    @Test
    fun getProductListTest() {
        given(productListService.getProductList()).willReturn(SingleList())

        val productListObservable = productListDataStoreImpl.getProductList()
        val productList = productListObservable.blockingGet()

        assertThat(productList[0], `is`<Any>(instanceOf<Any>(CarProduct::class.java)))
        assertThat(productList[1], `is`<Any>(instanceOf<Any>(ConsumerGoodProduct::class.java)))
        assertThat(productList[2], `is`<Any>(instanceOf<Any>(ServiceProduct::class.java)))
    }

    class SingleList : Single<List<ProductListDTO>>() {
        override fun subscribeActual(observer: SingleObserver<in List<ProductListDTO>>) {
            observer.onSuccess(createProductList())
        }

        private fun createProductList(): List<ProductListDTO> = listOf(createProductListDTO("car"),
                createProductListDTO("consumer_goods"),
                createProductListDTO("service"))

        private fun createProductListDTO(kind: String) =
                ProductListDTO(kind, ProductListItemDTO("", "", "", "", "", "", "", 0, "", "", "", 0, "", 0))

    }
}