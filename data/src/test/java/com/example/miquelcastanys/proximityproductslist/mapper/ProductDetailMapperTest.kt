package com.example.miquelcastanys.proximityproductslist.mapper

import com.example.data.entities.ProductListDTO
import com.example.data.entities.ProductListItemDTO
import com.example.data.entities.mappers.ProductDetailMapper
import com.example.domain.entities.CarProduct
import com.example.domain.entities.ConsumerGoodProduct
import com.example.domain.entities.ServiceProduct
import com.example.miquelcastanys.proximityproductslist.UnitTest
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Test

class ProductDetailMapperTest : UnitTest() {
    companion object {
        private const val CAR_ID = "1"
        private const val CONSUMER_GOOD_ID = "2"
        private const val SERVICE_ID = "3"
        private const val CAR_TYPE = "car"
        private const val CONSUMER_GOOD_TYPE = "consumer_goods"
        private const val SERVICE_TYPE = "service"
    }

    private val productList = listOf(createProductListDTO(CAR_ID, CAR_TYPE),
            createProductListDTO(CONSUMER_GOOD_ID, CONSUMER_GOOD_TYPE),
            createProductListDTO(SERVICE_ID, SERVICE_TYPE))


    @Test
    fun convertToTest() {
        val resultListCar = ProductDetailMapper.convertTo(CAR_ID, productList)

        Assert.assertThat(resultListCar,
                CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(CarProduct::class.java)))

        val resultListConsumerGood = ProductDetailMapper.convertTo(CONSUMER_GOOD_ID, productList)

        Assert.assertThat(resultListConsumerGood,
                CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(ConsumerGoodProduct::class.java)))

        val resultListService = ProductDetailMapper.convertTo(SERVICE_ID, productList)

        Assert.assertThat(resultListService,
                CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(ServiceProduct::class.java)))
    }

    private fun createProductListDTO(id: String, type: String) = ProductListDTO(type, createProductListItemDTO(id))

    private fun createProductListItemDTO(id: String) =
            ProductListItemDTO(id,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    0,
                    "",
                    "",
                    "",
                    0,
                    "",
                    0)
}