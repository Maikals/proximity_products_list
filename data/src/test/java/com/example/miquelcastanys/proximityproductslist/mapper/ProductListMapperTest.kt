package com.example.miquelcastanys.proximityproductslist.mapper

import com.example.data.entities.ProductListDTO
import com.example.data.entities.ProductListItemDTO
import com.example.data.entities.mappers.ProductListMapper
import com.example.data.net.ApiConstants
import com.example.domain.entities.AdvertisementProduct
import com.example.domain.entities.CarProduct
import com.example.domain.entities.ConsumerGoodProduct
import com.example.domain.entities.ServiceProduct
import com.example.miquelcastanys.proximityproductslist.UnitTest
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Test

class ProductListMapperTest : UnitTest() {

    private val productList = listOf(createProductListDTO(ApiConstants.CAR_TYPE),
            createProductListDTO(ApiConstants.CONSUMER_GOODS_TYPE),
            createProductListDTO(ApiConstants.SERVICE_TYPE))

    @Test
    fun convertToTest() {
        val resultList = ProductListMapper.convertTo(productList)

        Assert.assertThat(resultList[0], CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(CarProduct::class.java)))
        Assert.assertThat(resultList[1], CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(ConsumerGoodProduct::class.java)))
        Assert.assertThat(resultList[2], CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(ServiceProduct::class.java)))
        Assert.assertThat(resultList[3], CoreMatchers.`is`<Any>(CoreMatchers.instanceOf<Any>(AdvertisementProduct::class.java)))
        assert(resultList.size == 4)
    }

    private fun createProductListDTO(type: String, id: String = "") = ProductListDTO(type, createProductListItemDTO(id))

    private fun createProductListItemDTO(id: String = "") =
            ProductListItemDTO(id,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    0,
                    "",
                    "",
                    "",
                    0,
                    "",
                    0)
}