package com.example.data.net

object ApiConstants {
    const val TIMEOUT_CONNECTION_VALUE: Long = 10
    const val TIMEOUT_READ_VALUE: Long = 10
    const val TIMEOUT_WRITE_VALUE: Long = 10
    const val BASE_URL: String = "https://www.dropbox.com/s/cggseopytgwfra4/"
    const val API_GET_PRODUCT_LIST_PATH = "items.json?dl=1"
    const val CAR_TYPE: String = "car"
    const val CONSUMER_GOODS_TYPE: String = "consumer_goods"
    const val SERVICE_TYPE: String = "service"
    const val ADVERTISEMENT_TYPE: String = "advertisement"
}