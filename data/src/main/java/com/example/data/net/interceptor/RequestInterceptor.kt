package com.example.data.net.interceptor

import android.content.Context
import android.net.ConnectivityManager
import com.example.data.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RequestInterceptor @Inject constructor(private val context: Context): Interceptor {

    companion object {
        private const val NOT_CONNECTION_CODE = 600
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        if (BuildConfig.DEBUG) println("URl -> " + chain.request()?.url())

        if (!isThereInternetConnection()) {
            return Response.Builder()
                    .code(NOT_CONNECTION_CODE)
                    .request(chain.request())
                    .build()
        }
        return chain.proceed(chain.request())!!
    }

    private fun isThereInternetConnection(): Boolean {
        val isConnected: Boolean

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting

        return isConnected
    }
}