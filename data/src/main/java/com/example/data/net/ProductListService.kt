package com.example.data.net

import com.example.data.entities.ProductListDTO
import io.reactivex.Single
import retrofit2.http.GET


interface ProductListService {

    @GET(ApiConstants.API_GET_PRODUCT_LIST_PATH)
    fun getProductList() : Single<List<ProductListDTO>>

}