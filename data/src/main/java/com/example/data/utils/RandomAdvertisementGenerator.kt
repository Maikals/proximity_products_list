package com.example.data.utils

import com.example.domain.entities.AdvertisementProduct
import java.util.*

object RandomAdvertisementGenerator {

    fun generateRandomAdvertisement(): AdvertisementProduct =
            when (Random(Calendar.getInstance().timeInMillis).nextInt() % 3) {
                0 -> createCarAdvertisement()
                1 -> createServiceAdvertisement()
                else -> createConsumerGoodAdvertisement()
            }

    private fun createConsumerGoodAdvertisement(): AdvertisementProduct =
            AdvertisementProduct("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTshAaMrKU79urRwh9vuFRFty_wP19SmkM3CJnK5sPW5H8NQCMpZg",
                    "Want to get a new NES MINI?",
                    "https://google.com")

    private fun createServiceAdvertisement(): AdvertisementProduct =
            AdvertisementProduct("http://www.gamesgrabr.com/blog/wp-content/uploads/2015/09/hamster-NES.jpg",
                    "We play with your pets!",
                    "https://google.com")


    private fun createCarAdvertisement(): AdvertisementProduct =
            AdvertisementProduct("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAebBQrG7EzVs2pLuJscqbjzVRBpmi3rBUIRA_xv3yDMCIXnlvAg",
                    "Get a new car!",
                    "https://google.com")
}