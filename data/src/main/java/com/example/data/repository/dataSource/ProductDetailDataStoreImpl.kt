package com.example.data.repository.dataSource

import com.example.data.entities.mappers.ProductDetailMapper
import com.example.data.net.ProductListService
import com.example.domain.entities.baseEntities.BaseProduct
import io.reactivex.Single
import javax.inject.Inject

class ProductDetailDataStoreImpl@Inject constructor(private val productListService: ProductListService) : ProductDetailDataStore {
    override fun getProductDetail(productId: String): Single<BaseProduct> =
        productListService.getProductList().map { ProductDetailMapper.convertTo(productId, it) }


}