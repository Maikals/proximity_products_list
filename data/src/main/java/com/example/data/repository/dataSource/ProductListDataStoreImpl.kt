package com.example.data.repository.dataSource

import com.example.data.entities.mappers.ProductListMapper
import com.example.data.net.ProductListService
import com.example.domain.entities.baseEntities.BaseProduct
import io.reactivex.Single
import javax.inject.Inject


class ProductListDataStoreImpl @Inject constructor(private val productListService: ProductListService) : ProductListDataStore {
    override fun getProductList(): Single<List<BaseProduct>> =
            productListService
                    .getProductList()
                    .map { ProductListMapper.convertTo(it) }
}