package com.example.data.repository.dataSource

import com.example.domain.entities.baseEntities.BaseProduct
import io.reactivex.Single


interface ProductListDataStore {
    fun getProductList(): Single<List<BaseProduct>>
}