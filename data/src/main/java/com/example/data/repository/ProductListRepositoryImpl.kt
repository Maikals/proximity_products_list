package com.example.data.repository

import com.example.data.repository.dataSource.ProductListDataStore
import com.example.domain.entities.baseEntities.BaseProduct
import com.example.domain.repository.ProductListRepository
import io.reactivex.Single
import javax.inject.Inject


class ProductListRepositoryImpl @Inject constructor(private val dataStore: ProductListDataStore) : ProductListRepository {
    override fun getProductList(): Single<List<BaseProduct>> =
        dataStore.getProductList()
}