package com.example.data.repository.dataSource

import com.example.domain.entities.baseEntities.BaseProduct
import io.reactivex.Single


interface ProductDetailDataStore {
    fun getProductDetail(productId: String): Single<BaseProduct>
}