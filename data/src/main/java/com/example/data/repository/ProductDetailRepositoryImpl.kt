package com.example.data.repository

import com.example.data.repository.dataSource.ProductDetailDataStore
import com.example.data.repository.dataSource.ProductListDataStore
import com.example.domain.entities.baseEntities.BaseProduct
import com.example.domain.repository.ProductDetailRepository
import com.example.domain.repository.ProductListRepository
import io.reactivex.Single
import javax.inject.Inject

class ProductDetailRepositoryImpl @Inject constructor(private val dataStore: ProductDetailDataStore) : ProductDetailRepository {
    override fun getProductDetail(productId: String): Single<BaseProduct> =
        dataStore.getProductDetail(productId)
}