package com.example.data.entities


data class ProductListDTO(val kind: String?,
                          val item: ProductListItemDTO?)