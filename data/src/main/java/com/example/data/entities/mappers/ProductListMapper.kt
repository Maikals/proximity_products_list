package com.example.data.entities.mappers

import com.example.data.entities.ProductListDTO
import com.example.data.entities.mappers.base.BaseProductMapper
import com.example.data.utils.RandomAdvertisementGenerator
import com.example.domain.entities.baseEntities.BaseProduct
import kotlin.math.roundToInt

object ProductListMapper {
    private const val PERCENTAGE_OF_ADDS = 10.0

    fun convertTo(list: List<ProductListDTO>): List<BaseProduct> {
        val mappedList = list.filter { it.item?.id != null }
                .distinct()
                .sortedBy { it.item?.distanceInMeters }
                .map {
                    BaseProductMapper.createSpecificProductFromBase(it)
                }
        return addAdvertisements(mappedList)
    }

    /**
     * Returns a product list containing a PERCENTAGE_OF_ADDS
     *
     * @param productList The list of products where the advertisements are going to be added
     * @return A list with at least 1 advertisement
     */
    private fun addAdvertisements(productList: List<BaseProduct>): List<BaseProduct> {
        //We convert the mapped list into an ArrayList to modify it
        val resultList = ArrayList(productList)
        var i = 1
        //We add a calculateNumberOfAdds advertisements in the array.
        val addPosition = resultList.size / calculateNumberOfAdds(resultList.size)
        while (i * addPosition <= resultList.size) {
            resultList.add(i * addPosition, RandomAdvertisementGenerator.generateRandomAdvertisement())
            ++i
        }

        return resultList.toList()
    }

    /**
     * Returns the number of adds to show depending on the productListSize and the PERCENTAGE_OF_ADDS
     * constant.
     *
     * @param listSize The size of the list where the advertisements will be add
     * @return the number of advertisements to be added. The return value will be >= 1
     */
    private fun calculateNumberOfAdds(listSize: Int): Int {

        val numberOfAdds = (listSize * PERCENTAGE_OF_ADDS / 100.0).roundToInt()
        return if (numberOfAdds > 0) numberOfAdds else 1
    }
}