package com.example.data.entities.mappers

import com.example.data.entities.ProductListDTO
import com.example.data.entities.mappers.base.BaseProductMapper
import com.example.domain.entities.baseEntities.BaseProduct

object ProductDetailMapper {
    fun convertTo(productId: String, it: List<ProductListDTO>): BaseProduct =
        BaseProductMapper.createSpecificProductFromBase(it.filter { it.item?.id != null }
                .distinct()
                .findLast { it.item?.id == productId }!!)
}
