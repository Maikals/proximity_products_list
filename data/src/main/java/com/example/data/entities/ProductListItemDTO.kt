package com.example.data.entities


data class ProductListItemDTO(val id: String?,
                              val image: String?,
                              val price: String?,
                              val name: String?,
                              val motor: String?,
                              val gearbox: String?,
                              val brand: String?,
                              val km: Int?,
                              val description: String?,
                              val color: String?,
                              val closeDay: String?,
                              val minimumAge: Int?,
                              val category: String?,
                              val distanceInMeters: Int?,
                              val advertisementUrl: String? = "")