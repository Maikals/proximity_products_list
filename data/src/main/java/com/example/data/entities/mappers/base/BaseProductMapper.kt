package com.example.data.entities.mappers.base

import com.example.data.entities.ProductListDTO
import com.example.data.entities.ProductListItemDTO
import com.example.data.net.ApiConstants
import com.example.domain.entities.CarProduct
import com.example.domain.entities.ConsumerGoodProduct
import com.example.domain.entities.ServiceProduct
import com.example.domain.entities.baseEntities.BaseProduct

object BaseProductMapper {

    fun createSpecificProductFromBase(productListDTO: ProductListDTO): BaseProduct =
            when (productListDTO.kind) {
                ApiConstants.CAR_TYPE -> createCarProduct(productListDTO.item!!)
                ApiConstants.CONSUMER_GOODS_TYPE -> createConsumerGoodsProduct(productListDTO.item!!)
                else -> createServiceProduct(productListDTO.item!!)
            }


    fun createServiceProduct(item: ProductListItemDTO): ServiceProduct =
            ServiceProduct(item.id ?: "",
                    item.image ?: "",
                    item.price ?: "",
                    item.name ?: "",
                    item.closeDay ?: "",
                    item.category ?: "",
                    item.minimumAge ?: 0,
                    item.description ?: "",
                    item.distanceInMeters ?: 0)

    fun createCarProduct(item: ProductListItemDTO): CarProduct =
            CarProduct(item.id ?: "",
                    item.image ?: "",
                    item.price ?: "",
                    item.name ?: "",
                    item.motor ?: "",
                    item.gearbox ?: "",
                    item.brand ?: "",
                    item.km ?: 0,
                    item.description ?: "",
                    item.distanceInMeters ?: 0)

    fun createConsumerGoodsProduct(item: ProductListItemDTO): ConsumerGoodProduct =
            ConsumerGoodProduct(item.id ?: "",
                    item.image ?: "",
                    item.price ?: "",
                    item.name ?: "",
                    item.color ?: "",
                    item.category ?: "",
                    item.description ?: "",
                    item.distanceInMeters ?: 0)
}