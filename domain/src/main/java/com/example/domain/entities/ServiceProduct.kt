package com.example.domain.entities

import com.example.domain.entities.baseEntities.BaseProduct


data class ServiceProduct(val id: String,
                          val image: String,
                          val price: String,
                          val name: String,
                          val closeDay: String,
                          val category: String,
                          val minimumAge: Int,
                          val description: String,
                          val distanceInMeters: Int) : BaseProduct(id, image, price, name, description, distanceInMeters)