package com.example.domain.entities

import com.example.domain.entities.baseEntities.BaseProduct


data class ConsumerGoodProduct (val id: String,
                                val image: String,
                                val price: String,
                                val name: String,
                                val color: String,
                                val category: String,
                                val description: String,
                                val distanceInMeters: Int): BaseProduct(id, image, price, name, description, distanceInMeters)