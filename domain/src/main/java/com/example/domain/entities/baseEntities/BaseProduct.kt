package com.example.domain.entities.baseEntities


abstract class BaseProduct(id: String, image: String, price: String, name: String, description: String, distanceInMeters: Int)