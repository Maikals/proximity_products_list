package com.example.domain.entities

import com.example.domain.entities.baseEntities.BaseProduct

data class CarProduct(val id: String,
                      val image: String,
                      val price: String,
                      val name: String,
                      val motor: String,
                      val gearbox: String,
                      val brand: String,
                      val km: Int,
                      val description: String,
                      val distanceInMeters: Int): BaseProduct(id, image, price, name, description, distanceInMeters)