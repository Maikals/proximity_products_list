package com.example.domain.entities.params

data class ProductDetailParams(val productId: String)