package com.example.domain.entities

import com.example.domain.entities.baseEntities.BaseProduct

data class AdvertisementProduct(val image: String,
                                val name: String,
                                val advertisementUrl: String) : BaseProduct("", image,"", name, "", 0)