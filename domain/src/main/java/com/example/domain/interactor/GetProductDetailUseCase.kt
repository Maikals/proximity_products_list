package com.example.domain.interactor

import com.example.domain.entities.params.ProductDetailParams
import com.example.domain.entities.baseEntities.BaseProduct
import com.example.domain.executor.PostExecutionThread
import com.example.domain.interactor.baseInteractor.BaseUseCase
import com.example.domain.repository.ProductDetailRepository
import io.reactivex.Single
import javax.inject.Inject


class GetProductDetailUseCase @Inject constructor(private val repository: ProductDetailRepository,
                                                  postExecutionThread: PostExecutionThread) :
        BaseUseCase<BaseProduct, ProductDetailParams>(postExecutionThread) {

    override fun buildUseCaseObservable(params: ProductDetailParams?): Single<BaseProduct> =
            repository.getProductDetail(params?.productId!!)
}