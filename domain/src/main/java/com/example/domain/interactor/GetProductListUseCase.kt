package com.example.domain.interactor

import com.example.domain.entities.baseEntities.BaseProduct
import com.example.domain.executor.PostExecutionThread
import com.example.domain.interactor.baseInteractor.BaseUseCase
import com.example.domain.repository.ProductListRepository
import io.reactivex.Single
import javax.inject.Inject


class GetProductListUseCase @Inject constructor(private val repository: ProductListRepository,
                                                postExecutionThread: PostExecutionThread) : BaseUseCase<List<BaseProduct>, Void>(postExecutionThread) {
    override fun buildUseCaseObservable(params: Void?): Single<List<BaseProduct>> =
            repository.getProductList()

}