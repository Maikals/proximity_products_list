package com.example.domain.repository

import com.example.domain.entities.baseEntities.BaseProduct
import io.reactivex.Single


interface ProductListRepository {

    fun getProductList(): Single<List<BaseProduct>>
}