package com.example.domain.repository

import com.example.domain.entities.baseEntities.BaseProduct
import io.reactivex.Single


interface ProductDetailRepository {

    fun getProductDetail(productId: String): Single<BaseProduct>
}